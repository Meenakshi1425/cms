class AddFeaturedToPages < ActiveRecord::Migration[6.0]
  def change
    add_column :pages, :featured, :boolean
  end
end
